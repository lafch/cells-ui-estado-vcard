import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import styles from './cells-ui-estado-vcard-styles.js';

import '@bbva-web-components/bbva-button-default';
import '@vcard-components/cells-util-behavior-vcard';

const cellsUtilBehaviorVcard = CellsBehaviors.cellsUtilBehaviorVcard;
/**
This component ...

Example:

```html
<cells-ui-estado-vcard></cells-ui-estado-vcard>
```

##styling-doc

* @customElement
* @polymer
* @LitElement
* @demo demo/index.html
*/
export class CellsUiEstadoVcard  extends cellsUtilBehaviorVcard(LitElement) {
  static get is() {
    return 'cells-ui-estado-vcard';
  }

  // Declare properties
  static get properties() {
    return {
      allPrivilegio: { type: String },
      lstEstado: {type: Array },
      lstEstadoPrivilegio: {type: Array},
      activeEstilo: {type: String },
      numeroStep:{type: Number},
      detail:{type: Object},
      isVisibleActionStep:{type: Boolean},
      user:{type: Object}
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.detail = new Object();
    this.estadoActivo = new Object();
    this.lstEstado = []; 
    this.allPrivilegio = 'ALL';
    this.lstEstadoPrivilegio = [];
    this.numeroStep = 0;   
    this.activeEstilo = '';
    this.isVisibleActionStep = true;
  }

  async setNumeroStep(detail){
    this.detail          = detail;
    this.estadoActivo    = detail && detail.estado && detail.estado.codigo ? detail.estado.codigo : ''; 
    let perfilPrivilegio = this.user && this.user.perfil && this.user.perfil.valor ? (this.user.perfil.valor == this.allPrivilegio ? this.allEstado : this.user.perfil.valor.split('/')) : [];
    
    var lstEstadoTemp = [];
    this.lstEstado.forEach((estado,index) => {
      if(estado.codigo == this.estadoActivo){
        let estadoActivoTemp = estado;
        estadoActivoTemp.activo = true;
        lstEstadoTemp.push(estadoActivoTemp);
        this.numeroStep = index;
      }else{
        let estadoDesactivadoTemp = estado;
        estadoDesactivadoTemp.activo = false;
        lstEstadoTemp.push(estadoDesactivadoTemp);
      }
      let indexPrivilegio = perfilPrivilegio.indexOf(estado.codigo) > -1 ? index : -1;
      if(indexPrivilegio > -1){
        this.lstEstadoPrivilegio.push(indexPrivilegio);
      }
    });

    this.lstEstado = lstEstadoTemp;
    this.isActiveNextStep;
    await this.requestUpdate();
  }

  get isActiveNextStep(){  
    return   this.lstEstadoPrivilegio.length > 0 && this.lstEstadoPrivilegio.indexOf(this.numeroStep) > -1 && this.numeroStep < this.lstEstado.length-1;
  }

  get allEstado(){
    var allEstado = [];
    this.lstEstado.forEach(estado => {
      allEstado.push(estado.codigo);
    });
    return allEstado;
  }


  nextStep(){
    this.numeroStep = this.numeroStep + 1;
    this.isActiveNextStep;

    var lstEstadoTmp = [];
    this.lstEstado.forEach((estado,index) => {
      let estadoTemp = estado;
      estadoTemp.activo = (this.numeroStep == index);
      lstEstadoTmp.push(estadoTemp);
    });
    this.lstEstado = lstEstadoTmp;
  }

  promoverBody(){
    var numeroStepTemp = this.numeroStep + 1;
    var estadoChange = new Object();
 
    this.lstEstado.forEach((estado,index) => {
      if((numeroStepTemp == index)){
        estadoChange = estado;
      }
    });
    delete estadoChange.activo;
    console.log(estadoChange.codigo);
    
    this.detail.estado = estadoChange;
  }


  promoverEstado() {
    this.promoverBody();
    this.dispatch(this.events.promoverEstadoSolicitudEvent, this.detail);
  }

  static get shadyStyles() {
    return `
      ${styles.cssText}
      ${getComponentSharedStyles('cells-ui-estado-vcard-shared-styles').cssText}
    `;
  }

  htmlStep(){
    return html`
      <div class='container'>
        <ul class="progressbar">
          ${this.lstEstado.map(estado => html`<li class='${estado.activo ? 'active':''}'>${estado.nombre}</li>` )}
        </ul>
      </div>
    `;
  }

  open(event) {
    // this.tarjetas = event.detail;
    this.getById('modal').open();
  }

  get getCodigoOficina(){
    return this.user && this.user.oficina && this.user.oficina.nombre ? this.user.oficina.nombre: '';
  }

  get getNombreCompleto(){
    return this.user && this.user.nombreCompleto ? this.user.nombreCompleto : '';
  }

  get getNumeroSolicitud(){
    return this.detail  && this.detail.codigo ? this.detail.codigo : '';
  }

  // Define a template
  render() {
    return html`
      <style>${this.constructor.shadyStyles}</style>
      <div class='contenedor'>
        <div class='estado-header'>
          <div>
            <label class="titulo">Datos de solicitante  </label>
          </div>
          <div class='item-header'>
            <span class='sub-titulo'>Oficina: </span><span>${this.getCodigoOficina}</span>
          </div>
          <div class='item-header'>
            <span class='sub-titulo'>Solicitante: </span><span>${this.getNombreCompleto}</span>
          </div>
          <div class='item-header'>
            <span class='sub-titulo'>Número de solicitud: </span><span>${this.getNumeroSolicitud}</span>
          </div>
        </div>
        <div class='estado-body'>
          <div class='estado-body-header'>
            <label class='titulo'>Estado solicitud  </label>
          </div>
          ${this.htmlStep()}
        </div>
        ${this.isActiveNextStep ? html`
        <div class='estado-footer'>
          <bbva-button-default id='btnNextStep'  @click='${ ()=>{this.promoverEstado()}}' text='Siguiente Estado'></bbva-button-default>
        </div>` : ''}
      </div>
    `;
  }
}

// Register the element with the browser
customElements.define(CellsUiEstadoVcard.is, CellsUiEstadoVcard);
